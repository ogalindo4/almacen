import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GetEstibaComponent } from './get-estiba/get-estiba.component';

interface Service {
  name: string;
}

interface Nivel {
  codigo: string;
}

interface Estiba {
  codigo: string;
  nivel: number;
  posicion: number;
}

interface Columna {
  posicion: number;
  codigo: string;
  nivelMaximo: number;
  estibasMaximas: number;
  estibas: Estiba[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  estantes: any[] = [
    {
      nombre: "Respuesto",
      prefijo: "A",
      createdAt: "hoy",
      username: "jespinel",
      userid: 1,
      bodega: "latelier",//esto va quemao porque no hay tiempo
      columnas: [
        {
          posicion: 1,
          codigo: "A1",
          nivelMaximo: 20,
          estibasMaximas: 2,
          estibas: [
            {
              codigo: "A111",
              nivel: 1,
              posicion: 1
            },
            {
              codigo: "A123",
              nivel: 2,
              posicion: 3
            },
            {
              codigo: "A112",
              nivel: 1,
              posicion: 2
            }
          ]
        },
        {
          posicion: 2,
          codigo: "A2",
          nivelMaximo: 2,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A211",
              nivel: 1,
              posicion: 1
            },
            {
              codigo: "A212",
              nivel: 1,
              posicion: 1
            },
          ]
        },
        {
          posicion: 3,
          codigo: "A3",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "A4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "A4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "A4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "A4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "A4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "A4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "A4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "A341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        }
      ]
    },
    {
      nombre: "Pasillo",
      prefijo: "B",
      createdAt: "hoy",
      username: "jespinel",
      userid: 1,
      bodega: "latelier",//esto va quemao porque no hay tiempo
      columnas: [
        {
          posicion: 1,
          codigo: "B1",
          nivelMaximo: 8,
          estibasMaximas: 2,
          estibas: [
            {
              codigo: "B111",
              nivel: 1,
              posicion: 1
            },
            {
              codigo: "B123",
              nivel: 2,
              posicion: 3
            },
            {
              codigo: "B112",
              nivel: 3,
              posicion: 2
            }
          ]
        },
        {
          posicion: 2,
          codigo: "B2",
          nivelMaximo: 5,
          estibasMaximas: 3,
          estibas: [
            {
              codigo: "B211",
              nivel: 1,
              posicion: 1
            },
            {
              codigo: "B212",
              nivel: 1,
              posicion: 1
            },
          ]
        },
        {
          posicion: 3,
          codigo: "B3",
          nivelMaximo: 4,
          estibasMaximas: 2,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            }
          ]
        },
        {
          posicion: 4,
          codigo: "B4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "A342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "B4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "B342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "B4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "B342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "B4",
          nivelMaximo: 6,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "B342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "B4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "B342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "B4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "B342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        },
        {
          posicion: 4,
          codigo: "B4",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "B341",
              nivel: 4,
              posicion: 2
            },
            {
              codigo: "B342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        }
      ]
    },
    {
      nombre: "Pernos",
      prefijo: "C",
      createdAt: "hoy",
      username: "jespinel",
      userid: 1,
      bodega: "latelier",//esto va quemao porque no hay tiempo
      columnas: [
        {
          posicion: 1,
          codigo: "C1",
          nivelMaximo: 3,
          estibasMaximas: 2,
          estibas: [
            {
              codigo: "C111",
              nivel: 1,
              posicion: 1
            },
            {
              codigo: "C112",
              nivel: 1,
              posicion: 2
            },
            {
              codigo: "C121",
              nivel: 2,
              posicion: 1
            },
            {
              codigo: "C122",
              nivel: 2,
              posicion: 2
            },
            {
              codigo: "C131",
              nivel: 3,
              posicion: 1
            },
            {
              codigo: "C132",
              nivel: 3,
              posicion: 2
            }
          ]
        },
        {
          posicion: 2,
          codigo: "C2",
          nivelMaximo: 2,
          estibasMaximas: 3,
          estibas: [
            {
              codigo: "C211",
              nivel: 1,
              posicion: 1
            },
            {
              codigo: "C212",
              nivel: 1,
              posicion: 1
            },
          ]
        },
        {
          posicion: 3,
          codigo: "C3",
          nivelMaximo: 5,
          estibasMaximas: 5,
          estibas: [
            {
              codigo: "C311",
              nivel: 1,
              posicion: 1
            },
            {
              codigo: "C312",
              nivel: 1,
              posicion: 2
            },
            {
              codigo: "C313",
              nivel: 1,
              posicion: 3
            },
            {
              codigo: "C314",
              nivel: 1,
              posicion: 4
            },
            {
              codigo: "C315",
              nivel: 1,
              posicion: 5
            },
            {
              codigo: "C321",
              nivel: 2,
              posicion: 1
            },
            {
              codigo: "C322",
              nivel: 2,
              posicion: 2
            },
            {
              codigo: "C323",
              nivel: 2,
              posicion: 3
            },
            {
              codigo: "C324",
              nivel: 2,
              posicion: 4
            },
            {
              codigo: "C325",
              nivel: 2,
              posicion: 5
            },
            {
              codigo: "C342",
              nivel: 4,
              posicion: 2
            }
  
          ]
        }
      ]
    }
  ];

  EstanteSelected: any = {columnas: []};

  nivelesEspecificos: Nivel[] = [];
  nombreCelda: string = 'A551';
  niveles = [];
  constructor(public dialog: MatDialog) {
    this.EstanteSelected = this.estantes[0];
  }

  fnGetLayoutLevels = (nLevels: number) =>
    nLevels ? Array.from(new Array(nLevels), (x, index) => index).reverse() : [];

  fnGetLayoutPallet = (nPallets: number) => nPallets ? new Array(nPallets) : [];

  fnFindPalletByColumnAndLevel = (column: Columna, level: number, estiba: number): any => {
    if (column.estibas) {
      const palletsByLevel = column.estibas.filter(e => e.nivel == level);
      if (palletsByLevel.length == 0) return null;
      const palletsByPosition = palletsByLevel.filter(p => p.posicion == estiba);
      if (palletsByPosition.length == 0) return null;
      else return palletsByPosition[0];
    } else {
      return null;
    }
  }

  getPalletsName = (pallet: any = null) => {

    return pallet ? pallet.codigo : '';
  }

  openDialog(columna: any) {
    const dialogRef = this.dialog.open(GetEstibaComponent);
    
    dialogRef.afterClosed().subscribe(result => {
        if (result) {
            console.log(result);
            columna.estibas.push(result);
        }
    }); 
  }
}
