import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetEstibaComponent } from './get-estiba.component';

describe('GetEstibaComponent', () => {
  let component: GetEstibaComponent;
  let fixture: ComponentFixture<GetEstibaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetEstibaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetEstibaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
