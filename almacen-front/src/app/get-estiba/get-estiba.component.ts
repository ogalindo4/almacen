import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-get-estiba',
  templateUrl: './get-estiba.component.html',
  styleUrls: ['./get-estiba.component.scss']
})
export class GetEstibaComponent implements OnInit {
  profileForm = this.fb.group({
    codigo: new FormControl('', Validators.required),
    nivel: new FormControl('', Validators.required),
    posicion: new FormControl('', Validators.required),
  });
  estiba: any = {
    codigo: '',
    nivel: null,
    posicion: null
  };
  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<GetEstibaComponent>) {
    
   }

  ngOnInit(): void {
  }

  onSave() {
    this.dialogRef.close(this.estiba);
  }

}
